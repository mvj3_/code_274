# SecureRandom.hex(n)
#The argument n specifies the length of the random length. The length of the result string is twice of n.
#http://ruby-doc.org/stdlib-1.9.2/libdoc/securerandom/rdoc/SecureRandom.html
ice@mac:eoel > ruby -v
ruby 1.9.3p165 (2012-03-18 revision 35078) [x86_64-darwin11.3.0]
ice@mac:/source/1sters/tagskill/tagskill > irb
1.9.3p165 :001 > require 'securerandom'
 => true 
1.9.3p165 :002 > SecureRandom.hex(13)
 => "303b9efa3497097f4ea3587e3a" 
1.9.3p165 :003 > SecureRandom.hex(1)
 => "19" 
1.9.3p165 :004 > SecureRandom.hex(3)
 => "68586d" 
1.9.3p165 :005 > SecureRandom.hex(4)
 => "8de8966a" 
1.9.3p165 :006 > SecureRandom.hex(3)
 => "3786b5" 